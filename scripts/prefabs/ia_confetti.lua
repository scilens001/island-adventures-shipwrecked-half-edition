local SLOTMACHINE_SHAPE_TRIS = {
    {{x=1.0035,y=1.225},{x=0.3245,y=0.849},{x=1.2085,y=0.903},},
    {{x=0.2685,y=0.3715},{x=0.1595,y=0.316},{x=0.0755,y=0.8665},},
    {{x=1.125,y=1.388},{x=0.95,y=1.192},{x=0.4935,y=1.0295},},
    {{x=0.5095,y=0.236},{x=0.9815,y=1.016},{x=1.0985,y=1.268},},
    {{x=0.6705,y=0.704},{x=1.3735,y=0.654},{x=1.039,y=1.2975},},
    {{x=0.9215,y=0.196},{x=1.347,y=0.324},{x=0.513,y=1.2445},},
    {{x=1.0635,y=0.779},{x=0.3225,y=0.569},{x=0.075,y=0.5675},},
    {{x=0.623,y=0.5275},{x=0.3135,y=0.3825},{x=1.1075,y=1.186},},
    {{x=0.6805,y=0.3985},{x=1.091,y=0.2545},{x=1.4685,y=0.582},},
    {{x=0.659,y=0.5415},{x=1.232,y=1.09},{x=0.942,y=1.182},},
    {{x=0.7815,y=0.848},{x=0.1755,y=0.2135},{x=1.331,y=0.7405},},
    {{x=1.244,y=0.082},{x=0.8815,y=0.748},{x=0.2965,y=0.6655},},
    {{x=1.1355,y=1.176},{x=0.503,y=0.8845},{x=1.0935,y=0.618},},
    {{x=1.025,y=0.49},{x=0.4885,y=0.929},{x=0.303,y=0.3735},},
    {{x=0.6795,y=0.344},{x=0.874,y=0.1505},{x=1.068,y=0.583},},
    {{x=0.266,y=0.8755},{x=1.263,y=0.705},{x=1.054,y=1.0435},},
    {{x=0.7165,y=0.5105},{x=0.321,y=0.6125},{x=0.6905,y=0.781},},
    {{x=0.916,y=0.0795},{x=1.1825,y=0.168},{x=0.3725,y=0.111},},
    {{x=0.499,y=1.0795},{x=0.4915,y=0.0485},{x=0.4765,y=0.6175},},
    {{x=0.5215,y=0.0795},{x=1.0825,y=0.084},{x=0.3725,y=0.0555},},
    {{x=0.8555,y=1.3695},{x=0.378,y=1.133},{x=1.092,y=1.019},},
    {{x=0.5755,y=0.7095},{x=0.4345,y=0.261},{x=0.947,y=0.713},},
    {{x=0.6895,y=1.194},{x=0.6815,y=1.1995},{x=0.255,y=0.9375},},
    {{x=0.674,y=0.5355},{x=0.322,y=0.982},{x=1.1,y=1.003},},
    {{x=0.464,y=0.283},{x=1.183,y=1.041},{x=0.575,y=0.7205},},

    {{x=-1.0035,y=-1.225},{x=-0.3245,y=-0.849},{x=-1.2085,y=-0.903},},
    {{x=-0.2685,y=-0.3715},{x=-0.1595,y=-0.316},{x=-0.0755,y=-0.8665},},
    {{x=-1.125,y=-1.388},{x=-0.95,y=-1.192},{x=-0.4935,y=-1.0295},},
    {{x=-0.5095,y=-0.236},{x=-0.9815,y=-1.016},{x=-1.0985,y=-1.268},},
    {{x=-0.6705,y=-0.704},{x=-1.3735,y=-0.654},{x=-1.039,y=-1.2975},},
    {{x=-0.9215,y=-0.196},{x=-1.347,y=-0.324},{x=-0.513,y=-1.2445},},
    {{x=-1.0635,y=-0.779},{x=-0.3225,y=-0.569},{x=-0.075,y=-0.5675},},
    {{x=-0.623,y=-0.5275},{x=-0.3135,y=-0.3825},{x=-1.1075,y=-1.186},},
    {{x=-0.6805,y=-0.3985},{x=-1.091,y=-0.2545},{x=-1.4685,y=-0.582},},
    {{x=-0.659,y=-0.5415},{x=-1.232,y=-1.09},{x=-0.942,y=-1.182},},
    {{x=-0.7815,y=-0.848},{x=-0.1755,y=-0.2135},{x=-1.331,y=-0.7405},},
    {{x=-1.244,y=-0.082},{x=-0.8815,y=-0.748},{x=-0.2965,y=-0.6655},},
    {{x=-1.1355,y=-1.176},{x=-0.503,y=-0.8845},{x=-1.0935,y=-0.618},},
    {{x=-1.025,y=-0.49},{x=-0.4885,y=-0.929},{x=-0.303,y=-0.3735},},
    {{x=-0.6795,y=-0.344},{x=-0.874,y=-0.1505},{x=-1.068,y=-0.583},},
    {{x=-0.266,y=-0.8755},{x=-1.263,y=-0.705},{x=-1.054,y=-1.0435},},
    {{x=-0.7165,y=-0.5105},{x=-0.321,y=-0.6125},{x=-0.6905,y=-0.781},},
    {{x=-0.916,y=-0.0795},{x=-1.1825,y=-0.168},{x=-0.3725,y=-0.111},},
    {{x=-0.499,y=-1.0795},{x=-0.4915,y=-0.0485},{x=-0.4765,y=-0.6175},},
    {{x=-0.5215,y=-0.0795},{x=-1.0825,y=-0.084},{x=-0.3725,y=-0.0555},},
    {{x=-0.8555,y=-1.3695},{x=-0.378,y=-1.133},{x=-1.092,y=-1.019},},
    {{x=-0.5755,y=-0.7095},{x=-0.4345,y=-0.261},{x=-0.947,y=-0.713},},
    {{x=-0.6895,y=-1.194},{x=-0.6815,y=-1.1995},{x=-0.255,y=-0.9375},},
    {{x=-0.674,y=-0.5355},{x=-0.322,y=-0.982},{x=-1.1,y=-1.003},},
    {{x=-0.464,y=-0.283},{x=-1.183,y=-1.041},{x=-0.575,y=-0.7205},},
    
    {{x=-1.0035,y=1.225},{x=-0.3245,y=0.848},{x=-1.2085,y=0.903},},
    {{x=-0.2685,y=0.3715},{x=-0.1595,y=0.316},{x=-0.0755,y=0.8665},},
    {{x=-1.125,y=1.388},{x=-0.950,y=1.192},{x=-0.4935,y=1.0295},},
    {{x=-0.5095,y=0.236},{x=-0.9815,y=1.016},{x=-1.0985,y=1.268},},
    {{x=-0.6705,y=0.704},{x=-1.3735,y=0.654},{x=-1.039,y=1.2975},},
    {{x=-0.9215,y=0.196},{x=-1.347,y=0.324},{x=-0.513,y=1.2445},},
    {{x=-1.0635,y=0.779},{x=-0.3225,y=0.2845},{x=-0.075,y=0.284},},
    {{x=-0.623,y=0.5275},{x=-0.3135,y=0.3825},{x=-1.1075,y=1.186},},
    {{x=-0.6805,y=0.3985},{x=-1.091,y=0.2545},{x=-1.4685,y=0.582},},
    {{x=-0.659,y=0.5415},{x=-1.232,y=1.090},{x=-0.942,y=1.182},},
    {{x=-0.7815,y=0.848},{x=-0.1755,y=0.2135},{x=-1.331,y=0.7405},},
    {{x=-1.244,y=0.082},{x=-0.8815,y=0.748},{x=-0.2965,y=0.6655},},
    {{x=-1.1355,y=1.176},{x=-0.503,y=0.8845},{x=-1.0935,y=0.309},},
    {{x=-1.025,y=0.49},{x=-0.4885,y=0.929},{x=-0.303,y=0.8735},},
    {{x=-0.6795,y=0.344},{x=-0.874,y=0.1505},{x=-1.068,y=0.583},},
    {{x=-0.266,y=0.8755},{x=-1.263,y=0.705},{x=-1.054,y=1.0435},},
    {{x=-0.7165,y=0.5105},{x=-0.321,y=0.6125},{x=-0.3405,y=0.2615},},
    {{x=-0.2615,y=0.349},{x=-0.9505,y=1.2305},{x=-0.6905,y=1.281},},
    {{x=-0.916,y=0.777},{x=-0.2775,y=0.3045},{x=-0.229,y=0.927},},
    {{x=-0.235,y=0.282},{x=-1.3825,y=0.1035},{x=-0.391,y=0.4385},},
    {{x=-0.099,y=1.0795},{x=-0.4915,y=0.0485},{x=-0.4765,y=0.6175},},
    {{x=-0.5215,y=0.0795},{x=-1.0825,y=0.168},{x=-0.3725,y=0.111},},
    {{x=-0.8555,y=1.3695},{x=-0.378,y=1.133},{x=-1.092,y=1.019},},
    {{x=-0.5755,y=0.7095},{x=-0.4345,y=0.261},{x=-0.947,y=0.713},},
    {{x=-0.6895,y=1.194},{x=-0.6815,y=1.1995},{x=-0.255,y=0.9375},},
    {{x=-0.674,y=0.5355},{x=-0.322,y=0.982},{x=-1.1,y=1.003},},
    {{x=-0.464,y=0.283},{x=-1.183,y=1.041},{x=-0.575,y=0.7205},},

    {{x=1.0035,y=-1.225},{x=0.3245,y=-0.849},{x=1.2085,y=-0.903},},
    {{x=0.2685,y=-0.3715},{x=0.1595,y=-0.316},{x=0.0755,y=-0.8665},},
    {{x=1.125,y=-1.388},{x=0.95,y=-1.192},{x=0.4935,y=-1.0295},},
    {{x=0.5095,y=-0.236},{x=0.9815,y=-1.016},{x=1.0985,y=-1.268},},
    {{x=0.6705,y=-0.704},{x=1.3735,y=-0.654},{x=1.039,y=-1.2975},},
    {{x=0.9215,y=-0.196},{x=1.347,y=-0.324},{x=0.513,y=-1.2445},},
    {{x=1.0635,y=-0.779},{x=0.3225,y=-0.569},{x=0.075,y=-0.5675},},
    {{x=0.623,y=-0.5275},{x=0.3135,y=-0.3825},{x=1.1075,y=-1.186},},
    {{x=0.6805,y=-0.3985},{x=1.091,y=-0.2545},{x=1.4685,y=-0.582},},
    {{x=0.659,y=-0.5415},{x=1.232,y=-1.09},{x=0.942,y=-1.182},},
    {{x=0.7815,y=-0.848},{x=0.1755,y=-0.2135},{x=1.331,y=-0.7405},},
    {{x=1.244,y=-0.082},{x=0.8815,y=-0.748},{x=0.2965,y=-0.6655},},
    {{x=1.1355,y=-1.176},{x=0.503,y=-0.8845},{x=1.0935,y=-0.618},},
    {{x=1.025,y=-0.49},{x=0.4885,y=-0.929},{x=0.303,y=-0.8735},},
    {{x=0.6795,y=-0.344},{x=0.874,y=-0.1505},{x=1.068,y=-0.583},},
    {{x=0.266,y=-0.8755},{x=1.263,y=-0.705},{x=1.054,y=-1.0435},},
    {{x=0.7165,y=-0.5105},{x=0.321,y=-0.6125},{x=0.3405,y=-0.2615},},
    {{x=0.2615,y=-0.349},{x=0.9505,y=-1.2305},{x=0.6905,y=-1.281},},
    {{x=0.916,y=-0.777},{x=0.2775,y=-0.3045},{x=0.229,y=-0.927},},
    {{x=0.235,y=-0.282},{x=1.3825,y=-0.1035},{x=0.391,y=-0.4385},},
    {{x=0.099,y=-1.0795},{x=0.4915,y=-0.0485},{x=0.4765,y=-0.6175},},
    {{x=0.5215,y=-0.0795},{x=1.0825,y=-0.168},{x=0.3725,y=-0.111},},
    {{x=0.8555,y=-1.3695},{x=0.378,y=-1.133},{x=1.092,y=-1.019},},
    {{x=0.5755,y=-0.7095},{x=0.4345,y=-0.261},{x=0.947,y=-0.713},},
    {{x=0.6895,y=-1.194},{x=0.6815,y=-1.1995},{x=0.255,y=-0.9375},},
    {{x=0.674,y=-0.5355},{x=0.322,y=-0.982},{x=1.1,y=-1.003},},
    {{x=0.464,y=-0.283},{x=1.183,y=-1.041},{x=0.575,y=-0.7205},}
}

local TEXTURE = "fx/confetti.tex"
local SPARK_TEXTURE = "fx/sparkle.tex"

local ADD_SHADER = "shaders/vfx_particle_add.ksh"
local SHADER = "shaders/vfx_particle.ksh"

local COLOUR_ENVELOPE_NAME = "confetti_colourenvelope"
local SCALE_ENVELOPE_NAME = "confetti_scaleenvelope"

local COLOUR_ENVELOPE_NAME_SPARK = "confetti_colourenvelope_spark"
local SCALE_ENVELOPE_NAME_SPARK = "confetti_scaleenvelope_spark"

local assets =
{
    Asset("IMAGE", TEXTURE),
    Asset("IMAGE", SPARK_TEXTURE),
    Asset("SHADER", SHADER),
    Asset("SHADER", ADD_SHADER),
}

--------------------------------------------------------------------------

local function IntColour(r, g, b, a)
    return { r / 255, g / 255, b / 255, a / 255 }
end

local function InitEnvelope()
    local envs = {}

    EnvelopeManager:AddColourEnvelope(COLOUR_ENVELOPE_NAME..0,
        {
            { 0, IntColour(255, 0, 0, 255) },
            { 0.5, IntColour(255, 0, 0, 255) },
            { 1, IntColour(255, 0, 0, 0) },
        }
    )

    EnvelopeManager:AddColourEnvelope(COLOUR_ENVELOPE_NAME..1,
        {
            { 0, IntColour(0, 200, 0, 255) },
            { 0.5, IntColour(0, 200, 0, 255) },
            { 1, IntColour(0, 200, 0, 0) },
        }
    )

    EnvelopeManager:AddColourEnvelope(COLOUR_ENVELOPE_NAME..2,
        {
            { 0, IntColour(21, 85, 203, 255) },
            { 0.5, IntColour(21, 85, 203, 255) },
            { 1, IntColour(21, 85, 203, 0) },
        }
    )

    EnvelopeManager:AddColourEnvelope(COLOUR_ENVELOPE_NAME..3,
        {
            { 0, IntColour(255, 255, 255, 255) },
            { 0.5, IntColour(255, 255, 255, 255) },
            { 1, IntColour(255, 255, 255, 0) },
        }
    )

    EnvelopeManager:AddColourEnvelope(COLOUR_ENVELOPE_NAME..4,
        {
            { 0, IntColour(233, 224, 44, 255) },
            { 0.5, IntColour(233, 224, 44, 255) },
            { 1, IntColour(233, 224, 44, 0) },
        }
    )


    local envs = {}

    local max_scale = .7
    local end_scale = .4
    local t = 0
    local step = .2
    while t + step < 1 do
        local s = Lerp( max_scale, end_scale, Clamp(2*t - 0.5, 0, 1) )
        table.insert(envs, { t, { s * 0.25, s } })
        t = t + step

        local s = Lerp( max_scale, end_scale, Clamp(2*t - 0.5, 0, 1))
        table.insert(envs, { t, { s, s * 0.2 } })
        t = t + step
    end
    table.insert(envs, { 1, { max_scale, max_scale * 0.6 } })

    EnvelopeManager:AddVector2Envelope( SCALE_ENVELOPE_NAME, envs )


    EnvelopeManager:AddColourEnvelope(
        COLOUR_ENVELOPE_NAME_SPARK,
        {
            { 0,    IntColour(255, 255, 255, 25) },
            { .05,   IntColour(255, 255, 255, 255) },
            { .8,   IntColour(128, 128, 128, 255) },
            { 1,    IntColour(0, 0, 0, 0) },
        }
    )
    local spark_max_scale = 2.5
    EnvelopeManager:AddVector2Envelope(
        SCALE_ENVELOPE_NAME_SPARK,
        {
            { 0,    { spark_max_scale, spark_max_scale } },
            { 1,    { spark_max_scale * 0.4, spark_max_scale * 0.4 } },
        }
    )


    InitEnvelope = nil
    IntColour = nil
end

--------------------------------------------------------------------------
local MAX_LIFETIME = 2.7

local function emit_confetti_fn(effect, i, ep, camera_right, camera_up)
    local lifetime = MAX_LIFETIME * (.5 + UnitRand() * .5)
    local px, py, pz = ep(camera_right, camera_up)
    local vx, vy, vz = px * 0.75, py * 0.75, pz * 0.75
    vy = vy + 1 --apply upward force to all confetti
    py = py + 1 --confetti spawns at origin + 2

    local angle = math.random() * 360
    local uv_offset = math.random(0, 3) * .25
    local ang_vel = (UnitRand() - 1) * 5

    effect:AddRotatingParticleUV(
        i,
        lifetime,           -- lifetime
        0, 3, 0,         -- position
        vx, vy, vz,         -- velocity
        angle, ang_vel,     -- angle, angular_velocity
        uv_offset, 0        -- uv offset
    )
end

local SPARK_MAX_LIFETIME = .2
local function emit_spark_fn(effect, spark_sphere_emitter)
    local vx, vy, vz = .05 * UnitRand(), .05 * UnitRand(), .05 * UnitRand()
    local lifetime = SPARK_MAX_LIFETIME * (0.7 + math.random() * .3)
    local px, py, pz = spark_sphere_emitter()
    py = py + 3.4

    local uv_offset = math.random(0, 3) * .25

    effect:AddParticleUV(
        5,
        lifetime,           -- lifetime
        px, py, pz,         -- position
        vx, vy, vz,         -- velocity
        uv_offset, 0        -- uv offset
    )
end

local NUM_EMITTERS = 4
local function do_emits(inst, effect, ep, spark_sphere_emitter)
    local c_down = TheCamera:GetPitchDownVec():Normalize()
    local c_right = TheCamera:GetRightVec():Normalize()

    local c_up = c_down:Cross(c_right):Normalize()

    for i = 0,NUM_EMITTERS do
        local num_to_emit = math.random(150, 200)
        while num_to_emit > 0 do
            emit_confetti_fn(effect, i, ep, c_right, c_up)
            num_to_emit = num_to_emit - 1
        end
    end

    local num_to_emit_spark = 3
    while num_to_emit_spark > 0 do
        emit_spark_fn(effect, spark_sphere_emitter)
        num_to_emit_spark = num_to_emit_spark - 1
    end

    inst:Remove()
end

local function MakeConfetti(name, data)
    local function fn()
        local inst = CreateEntity()

        inst.entity:AddTransform()
        inst.entity:AddNetwork()
        inst.entity:AddSoundEmitter()

        inst:AddTag("FX")

        inst.entity:SetPristine()

        inst.persists = false

        --Dedicated server does not need to spawn local particle fx
        if TheNet:IsDedicated() then
            return inst
        elseif InitEnvelope ~= nil then
            InitEnvelope()
        end

        local effect = inst.entity:AddVFXEffect()
        effect:InitEmitters(6)

        for i=0,NUM_EMITTERS do
            effect:SetRenderResources(i, TEXTURE, SHADER)
            effect:SetRotationStatus(i, true)
            effect:SetUVFrameSize(i, .25, 1)
            effect:SetMaxNumParticles(i, 200)
            effect:SetMaxLifetime(i, MAX_LIFETIME)
            effect:SetColourEnvelope(i, COLOUR_ENVELOPE_NAME..i)
            effect:SetScaleEnvelope(i, SCALE_ENVELOPE_NAME)
            effect:SetBlendMode(i, BLENDMODE.Premultiplied)
            effect:EnableBloomPass(i, true)
            effect:SetSortOrder(i, 0)
            effect:SetSortOffset(i, 0)
            effect:SetGroundPhysics(i, true)

            effect:SetAcceleration(i, 0, -0.8, 0)
            effect:SetDragCoefficient(i, .1)
        end

        --Sparkle
        effect:SetRenderResources(5, SPARK_TEXTURE, ADD_SHADER)
        effect:SetMaxNumParticles(5, 20)
        effect:SetMaxLifetime(5, SPARK_MAX_LIFETIME)
        effect:SetColourEnvelope(5, COLOUR_ENVELOPE_NAME_SPARK)
        effect:SetScaleEnvelope(5, SCALE_ENVELOPE_NAME_SPARK)
        effect:SetBlendMode(5, BLENDMODE.Additive)
        effect:EnableBloomPass(5, true)
        effect:SetUVFrameSize(5, 0.25, 1)
        effect:SetSortOrder(5, 0)
        effect:SetSortOffset(5, 1)
        effect:SetDragCoefficient(5, .11)

        local ep = Create2DTriEmitter(data.tri_list, 0.5)
        local spark_sphere_emitter = CreateSphereEmitter(.03)
        inst:DoTaskInTime(0, do_emits, effect, ep, spark_sphere_emitter)

        inst.SoundEmitter:PlaySound("yotb_2021/common/fireworks")

        return inst
    end

    return Prefab(name, fn, assets)
end

return MakeConfetti("slot_confetti_fx", {tri_list = SLOTMACHINE_SHAPE_TRIS})