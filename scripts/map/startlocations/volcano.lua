local STRINGS = GLOBAL.STRINGS

AddStartLocation("VolcanoPortal", {
	name = STRINGS.UI.SANDBOXMENU.VOLCANO,
	location = "forest",
	start_setpeice = "VolcanoPortal",
	start_node = "VolcanoNoise",
})
