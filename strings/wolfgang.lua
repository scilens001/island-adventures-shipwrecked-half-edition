--Wolfgang's speech file
return {

	ACTIONFAIL =
	{
		REPAIRBOAT =
		{
			GENERIC = "Small boat has no damages to repair! Wolfgang could punch it?",
		},
		EMBARK = 
		{
			INUSE = "Tiny ship left without Wolfgang!",
		},
		INSPECTBOAT = 
		{
			INUSE = GLOBAL.STRINGS.CHARACTERS.GENERIC.ACTIONFAIL.STORE.INUSE
		},
		OPEN_CRAFTING  = 
		{
			FLOODED = "Is too watery!",
		},
	},

	ANNOUNCE_MAGIC_FAIL = "Is not making the magics now. Why?",

	ANNOUNCE_SHARX = "Wolfgang has made new friends!",--unused

	ANNOUNCE_TREASURE = "Wolfgang sees presents!",
	ANNOUNCE_TREASURE_DISCOVER = "Wolfgang sees presents!",
	ANNOUNCE_MORETREASURE = "Wolfgang find more shinies!",
	ANNOUNCE_OTHER_WORLD_TREASURE = "Treasure does not look close by.",
	ANNOUNCE_OTHER_WORLD_PLANT = "Not mighty enough to grow here! Unlike Wolfgang!",

	ANNOUNCE_IA_MESSAGEBOTTLE =
	{
		"Eh... Wolfgang can not read.",
	},
	ANNOUNCE_VOLCANO_ERUPT = "Ha ha! Time to fight MOUNTAIN!",
	ANNOUNCE_MAPWRAP_WARN = "Is getting scary!",
	ANNOUNCE_MAPWRAP_LOSECONTROL = "Boat not listen to mighty Wolfgang!",
	ANNOUNCE_MAPWRAP_RETURN = "Wolfgang cannot be stopped!",
	ANNOUNCE_CRAB_ESCAPE = "Crabbit has fled. I do not blame it.",
	ANNOUNCE_TRAWL_FULL = "Net is fat with treasures!",
	ANNOUNCE_BOAT_DAMAGED = "Ship is weak! Needs fixingtimes.",
	ANNOUNCE_BOAT_SINKING = "I will fight sea!",
	ANNOUNCE_BOAT_SINKING_IMMINENT = "Wolfgang will punch death!",
	ANNOUNCE_WAVE_BOOST = "I am mighty!",

	ANNOUNCE_WHALE_HUNT_BEAST_NEARBY = "Wolfgang will find you, big fishie!",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL = "Argh! Big fishie outsmart Wolfgang!",
	ANNOUNCE_WHALE_HUNT_LOST_TRAIL_SPRING = "Wolfgang cannot follow big fishie!",

	DESCRIBE = {
		
		FLOTSAM = "Is not a mighty ship.",
		SUNKEN_BOAT =
		{
			ABANDONED = "No one home.",
			GENERIC = "Where is captain?",
		},
		SUNKEN_BOAT_BURNT = "Pile of burnt wood.",

		BOAT_LOGRAFT = "This raft is... not so mighty.",
		BOAT_RAFT = "How will tiny plank support Wolfgang?",
		BOAT_ROW = "I will row with powerful arms!",
		BOAT_CARGO = "What a mighty storage hold!",
		BOAT_ARMOURED = "You are mighty, just like me.",
		BOAT_ENCRUSTED = "Tiny boat will hold all of Wolfgang?",
		CAPTAINHAT = "Look at Wolfgang! He is captain now!",

		BOAT_TORCH = "Flame on boat is good idea!",
		BOAT_LANTERN = "Is safer light.",
		BOATREPAIRKIT = "Restore my mighty vessel!",
		BOATCANNON = "Is the mightiest weapon!",

		BOTTLELANTERN = "Is good way to keep darkness back.",
		BIOLUMINESCENCE = "Pretty sea-lights!",

		BALLPHIN = "Come and fight with me, sea-friends!", 
		BALLPHINHOUSE = "Friends of Wolfgang live inside.",
		DORSALFIN = "Where is rest of fish?",
		TUNACAN = "I will open with bare hands!",

		JELLYFISH = "If I punch jelly, it will become jam.",
		JELLYFISH_DEAD = "It stood no chance against Wolfgang.",
		JELLYFISH_COOKED = "Jelly looks tasty now.",
		JELLYFISH_PLANTED = "Can it arm wrestle?",
		JELLYJERKY = "Jerky is tough, like Wolfgang!",
		RAINBOWJELLYFISH = "Ah! Is pretty jelly!",
		RAINBOWJELLYFISH_PLANTED = "Very pretty jelly!",
		RAINBOWJELLYFISH_DEAD = "Good looking jelly is no look so good.",
		RAINBOWJELLYFISH_COOKED = "Food is many pretty colors!",
		JELLYOPOP = "Looks like candy, but tastes like meat!",

		CROCODOG = "Hairless puppy!",
		POISONCROCODOG = "Hairless puppy is yellow!",
		WATERCROCODOG = "Hairless puppy is blue!",

		PURPLE_GROUPER = "Wolfgang wish he swim like fish.",
		PIERROT_FISH = "Little fish is very dry!",
		NEON_QUATTRO = "Ha ha! Scales is chilly!",
		PURPLE_GROUPER_COOKED = "Wolfgang will eat tasty fish!",
		PIERROT_FISH_COOKED = "Little fish will make Wolfgang mighty!",
		NEON_QUATTRO_COOKED = "Toasty fish is good muscle fuel.",
		TROPICALBOUILLABAISSE = "Wolfgang does not know what to call this.",

		FISH_FARM = 
		{
			EMPTY = "Need little fishie eggs!",
			STOCKED = "Fishes is growing.",
			ONEFISH = "Wolfgang has fish!",
			TWOFISH = "More fishes for Wolfgang!",
			REDFISH = "Many fishes means fancy fish soup!",
			BLUEFISH  = "Wolfgang will eat ALL fishes!",
		},

		ROE = "Is eggy.",
		ROE_COOKED = "Little eggs is stink less now.",
		CAVIAR = "Little eggs make Wolfgang feel like big, important man.",

		CORMORANT = "Bird smells like fish eggs.",
		SEAGULL = "It has large mouth. Like me!",
		SEAGULL_WATER = "Resting after good fights, friend?",
		TOUCAN = "Bird flees at the sight of Wolfgang.",
		PARROT = "Chatty bird!",
		PARROT_PIRATE = "It could maybe be friend?",

		SEA_YARD = 
		{
			ON = "Will make boats mighty again!",
			OFF = "Is not working.",
			LOWFUEL = "Need fuel for running.",
		},
		
		SEA_CHIMINEA =
		{
			EMBERS = "Fire is sick, maybe?",
			GENERIC = "Wolfgang will make fire at sea.",
			HIGH = "Mighty fire for mighty Wolfgang!",
			LOW = "Little sea fire is look wimpy.",
			NORMAL = "Fire is okay.",
			OUT = "Wolfgang needs new fire.",
		}, 

		CHIMINEA = "Be safe from wind, little fire.",
		
		TAR_EXTRACTOR =
		{
			ON = "Machine is run strong.",
			OFF = "Is need on-switching.",
			LOWFUEL = "Floaty machine needs food.",											 
		},

		TAR = "Icky sticky black goo!",
		TAR_TRAP = "Goo cannot stop mighty Wolfgang feet!",
		TAR_POOL = "Is bubbling.",
		TARLAMP = "Lamp is so little, dainty.",
		TARSUIT = "Water not get Wolfgang now.",

		PIRATIHATITATOR =
		{
			GENERIC = "Strange contraption.",
			BURNT = "It does nothing.",
		},

		PIRATEHAT = "Ha ha! I like!",

		MUSSEL_FARM =
		{
			GENERIC = "Strangely named. They have no muscles.",
			STICKPLANTED = "Pathetic creatures, conquered by my stick!",
		},

		MUSSEL = "My muscles are much bigger!",
		MUSSEL_COOKED = "Mussels become protein for muscles!",
		MUSSELBOUILLABAISE = "It belongs in Wolfgang's mouth.",
		MUSSEL_BED = "Tiny mussels should be in water!",
		MUSSEL_STICK = "Mussels love little stick.",

		LOBSTER = "Wolfgang respects shellbeast's powerful claws.",
		LOBSTER_DEAD = "Wolfgang wish he had fearsome hand claws!",
		LOBSTER_DEAD_COOKED = "Is ready to eat now.",
		LOBSTERHOLE = "Is home for shellbeasts.",
		WOBSTERBISQUE = "Seems fancy.",
        WOBSTERDINNER = "A meal fit for Wolfgang!",
		SEATRAP = "Little pincher does not stand a chance.",

		BUSH_VINE =
		{
			BURNING = "Is firey!",
			BURNT = "Ash.",
			CHOPPED = "I won. You lost.",
			GENERIC = "This is bush. Of vines.",
		},

		VINE = "For string enemies together.",
		DUG_BUSH_VINE = "I will take it with me.",

		ROCK_LIMPET =
		{
			GENERIC = "Looks like tiny snails for eating!",
			PICKED = "Tiny snails are gone for now.",
		},
		
		LIMPETS = "Is snail? Maybe.",
		LIMPETS_COOKED = "It is slimy going down.",
		BISQUE = "It will fuel powerful punches!",

		MACHETE = "Ah! Very good!",
		GOLDENMACHETE = "Hoighty toighty knife!",

		THATCHPACK = "Wolfgang can carry more on powerful shoulders!",
		PIRATEPACK = "Sounds like jingles.",
		SEASACK = "Wolfgang can carry even more things!",

		SEAWEED_PLANTED =
        {
            GENERIC = "Strange leaves from sea.",
            PICKED = "All gone.",
        },

		SEAWEED = "A weed, from the sea.",
		SEAWEED_COOKED = "Wolfgang's strong stomach will destroy this!",
		SEAWEED_DRIED = "Is edible.",
		SEAWEED_STALK = "Squishy.",

		DUBLOON = "It dents when crushed between Wolfgang's powerful jaws!",
		SLOTMACHINE = "Wolfgang is gambling man!",

		SOLOFISH = "Face me, fish!",
		SOLOFISH_DEAD = "Is dead.",
		SWORDFISH = "Wolfgang challenges you to duel!",
		SWORDFISH_DEAD = "It was a good fight.",
		CUTLASS = "Wolfgang is ready for battle!",

		SUNKEN_BOAT_TRINKET_1 = "Wolfgang loves these!",
		SUNKEN_BOAT_TRINKET_2 = "Wolfgang will not fit!",
		SUNKEN_BOAT_TRINKET_3 = "It is the perfect amount of dampness.",
		SUNKEN_BOAT_TRINKET_4 = "Metal doodad is fun for playtimes.",
		SUNKEN_BOAT_TRINKET_5 = "Tough leather is hard to chew.",
		TRINKET_IA_13 = "Wolfgang would not like to drink.",
		TRINKET_IA_14 = "Wolfgang does not like that.",
		TRINKET_IA_15 = "It does not fit in Wolfgang's big hands.",
		TRINKET_IA_16 = "I do not know what this is.",
		TRINKET_IA_17 = "Tough leather is hard to chew.",
		TRINKET_IA_18 = "Is broken.",
		TRINKET_IA_19 = "Wolfgang is not eat loose pills.",
		TRINKET_IA_20 = "Wolfgang loves these!",
		TRINKET_IA_21 = "Wolfgang will not fit!",
		TRINKET_IA_22 = "Looks much too dry.",
		TRINKET_IA_23 = "Is very broken.",
		EARRING = "Someone drop this, maybe?",

		TURF_BEACH = "Step stones.",
		TURF_JUNGLE = "Step stones.",
		TURF_MAGMAFIELD = "Step stones.",
		TURF_TIDALMARSH = "Step stones.",
		TURF_MEADOW = "Step stones.",
		TURF_ASH = "Step stones.",
		TURF_VOLCANO = "Step stones.",
		TURF_SWAMP = "Step stones.",
		TURF_SNAKESKIN = "Step stones.",

		WHALE_BLUE = "I will eat you!",
		WHALE_CARCASS_BLUE = "Well fought, large fish!",
		WHALE_WHITE = "Great white! Fight Wolfgang!",
		WHALE_CARCASS_WHITE = "Not-so-great white.",
		WHALE_TRACK = "Massive fish cannot escape me!",
		WHALE_BUBBLES = "Large fishie is underneath.",
		BLUBBERSUIT = "Would rather wear a muscle suit!",
		BLUBBER = "Is not muscle.",
		HARPOON = "Even Wolfgang's throws is mighty!",

		SAIL_PALMLEAF = "Wolfgang will take to the seas.",
		SAIL_CLOTH = "I will capture you, wind!",
		SAIL_SNAKESKIN = "Skin of dead snake will help me sail.",
		SAIL_FEATHER = "Wolfgang is master of sea!",
		IRONWIND = "Spinny sail!",

		BERMUDATRIANGLE = "Is give me uneasy feelings.",

		PACKIM_FISHBONE = "Is bone. I like it.",
		PACKIM = "Hello, robust friend!",

		TIGERSHARK = "I will take you to POUND!",
		MYSTERYMEAT = "My mighty stomach can handle it!",
		SHARK_GILLS = "Maybe now Wolfgang breathe underwater? Maybe not.",
		TIGEREYE = "Is giving me dirty look.",
		DOUBLE_UMBRELLAHAT = "Two umbrellas is better than one.",
		SHARKITTEN = "Wolfgang is dog person.",
		SHARKITTENSPAWNER =
		{
			GENERIC = "Is time for cat nap?",
			INACTIVE = "Is just sand.",
		},

		WOODLEGS_KEY1 = "Spooky key!",
		WOODLEGS_KEY2 = "Pretty key!",--unused
		WOODLEGS_KEY3 = "Boring key!",--unused
		WOODLEGS_CAGE = "Is locked.",--unused

		CORAL = "Is very pretty! Will it crush?",
		ROCK_CORAL = "Is so fragile!",
		LIMESTONENUGGET = "Corals were crushed to make tiny rock.",
		NUBBIN = "Haha. Rock is bald.",
		CORALLARVE = "Is small and harmless.",
		WALL_LIMESTONE = "Good, sturdy wall.",
		WALL_LIMESTONE_ITEM = "I will make strong wall.",
		WALL_ENFORCEDLIMESTONE = "Is sea wall now!",
		WALL_ENFORCEDLIMESTONE_ITEM = "Wolfgang will build sea wall!",
		ARMORLIMESTONE = "This mighty armor will protect me!",
		CORAL_BRAIN_ROCK = "Wolfgang's brain will punch this brain.",
		CORAL_BRAIN = "Heh heh. It squish under Wolfgang's touch.",
		BRAINJELLYHAT = "Agh! Is making Wolfgang think!",

		SEASHELL = "Tiny shell would crush easily.",
		SEASHELL_BEACHED = "Wolfgang keep bowl of these on table at home.",
		ARMORSEASHELL = "Try your dirty tricks now, poison creatures.",

		ARMOR_LIFEJACKET = "Big seawater will not take Wolfgang now!",
		ARMOR_WINDBREAKER = "Wolfgang master of breaking wind!",

		SNAKE = "I am ready to fight!",
		SNAKE_POISON = "Poison is weapon of cowards!",
		SNAKEOIL = "Good for shining Wolfgang's muscles!",
		SNAKESKIN = "I will use as bandana.",
		SNAKESKINHAT = "Very good.",
		ARMOR_SNAKESKIN = "Is made to show my mighty muscles.",
		SNAKEDEN =
		{
			BURNING = "Is firey!",
			BURNT = "Ash.",
			CHOPPED = "I won. You lost.",
			GENERIC = "This is bush. Of vines.",
		},

		OBSIDIANFIREPIT =
		{
			EMBERS = "The darkness is coming!",
			GENERIC = "Wolfgang did good job!",
			HIGH = "Is too much fire!",
			LOW = "The fire is not hot enough.",
			NORMAL = "Hearty fire warms Wolfgang's bones.",
			OUT = "Uh oh. It turned off.",
		},

		OBSIDIAN = "There is strong fire in this stone.",
		ROCK_OBSIDIAN = "Very strong rock. Wolfgang would like to have it.",
		OBSIDIAN_WORKBENCH = "Is fire table.",
		OBSIDIANAXE = "The mightiest of axes!",
		OBSIDIANMACHETE = "Is so sharp!",
		SPEAR_OBSIDIAN = "Very strong, warm spear.",
		VOLCANOSTAFF = "Power of fire mountain is in Wolfgang's hands!",
		ARMOROBSIDIAN = "Is cheap trick, but Wolfgang like!",
		COCONADE =
		{
			BURNING = "Is burning up quickly.",
			GENERIC = "So much power!",
		},

		OBSIDIANCOCONADE =
		{
			BURNING = "Is burning up quickly.",
			GENERIC = "Even more might!",
		},

		VOLCANO_ALTAR =
		{
			GENERIC = "Is not wanting anything right now.",
			OPEN = "Wolfgang is afraid to use.",
		},

		VOLCANO = "Is calling me to fight!",
		VOLCANO_EXIT = "Goodbye!",
		ROCK_CHARCOAL = "Wolfgang rubs it under eyes to scare foes!",
		VOLCANO_SHRUB = "Tree was too weak. Dead now.",
		LAVAPOOL = "Sad. I forget swim trunks.",
		COFFEEBUSH =
		{
			BARREN = "Where did all tasty beans go?",
			WITHERED = "Is on death's door.",
			GENERIC = "Coffee bush is proud, strong.",
			PICKED = "Rest now, friend.",
		},

		COFFEEBEANS = "Tiny beans give Wolfgang strength!",
		COFFEEBEANS_COOKED = "Is bitter beans!",
		DUG_COFFEEBUSH = "It will grow no strong beans like this.",
		COFFEE = "Strong, like Wolfgang!",

		ELEPHANTCACTUS =
		{
			BARREN = "It needs ash.",
			WITHERED = "Is it dead?",
			GENERIC = "Ouch! Is still sharp.",
			PICKED = "Ha! It no longer sharp.",
		},

		DUG_ELEPHANTCACTUS = "I will pick it up with bare hands.",
		ELEPHANTCACTUS_ACTIVE = "Puny cactus wants to fight me!",
		ELEPHANTCACTUS_STUMP = "It is not dead, only sleeping.",
		NEEDLESPEAR = "The weapon of a mighty plant!",
		ARMORCACTUS = "Hah! Try hit Wolfgang now.",

		TWISTER = "What might!",
		TWISTER_SEAL = "Ha ha! Is good friend.",
		TURBINE_BLADES = "Blade that does not cut.",
		MAGIC_SEAL = "Why is sticky?",
		WINDSTAFF = "Stick makes good sailing!",
		WIND_CONCH = "Shell makes scary sounds!",

		DRAGOON = "Is time for flexing contest?",
		DRAGOONHEART = "Is mush in my hand.",
		DRAGOONSPIT = "Ha! Wolfgang spit much farther.",
		DRAGOONEGG = "Hatch so we might fight!",
		DRAGOONDEN = "Wolfgang approves.",

		ICEMAKER =
		{
			OUT = "Small machine is out of juice.",
			VERYLOW = "Machine will not last much longer.",
			LOW = "Little machine looks tired.",
			NORMAL = "Thank you for good ice, machine.",
			HIGH = "Machine is doing very good job!",
		},

		HAIL_ICE = "Is cold.",

		BAMBOOTREE =
		{
			BURNING = "Is burning!",
			BURNT = "Is not very mighty now.",
			CHOPPED = "I hope is fast growing.",
			GENERIC = "I will harvest you soon, mighty plant!",
		},

		BAMBOO = "Very mighty building material!",
		FABRIC = "Flimsy cloth-stuffs.",
		DUG_BAMBOOTREE = "I will take it with me.",

		JUNGLETREE =
		{
			BURNING = "I broke it.",
			BURNT = "Is small and broken now.",
			CHOPPED = "Ha! I win!",
			GENERIC = "Tree is strong and magnificent.",
		},

		JUNGLETREESEED = "It might become tree.",
		JUNGLETREESEED_SAPLING = "It has better chance of being tree now.",
		LIVINGJUNGLETREE = "Looks like ordinary tree.",

		OX = "What a mighty beast!",
		BABYOX = "He will grow very strong. Or she?",
		OX_HORN = "Horn of strong ox!",
		OXHAT = "I have become more mighty!",
		OX_FLUTE = "Wolfgang will play lusty tune.",

		MOSQUITO_POISON = "Fight like real man!",
		MOSQUITOSACK_YELLOW = "Is yellow punching bag.",

		STUNGRAY = "Looks smooth. I will pet it!",
		POISONHOLE = "Cowardly hole!",
		GASHAT = "Little hat is very colorful.",

		ANTIVENOM = "Wolfgang will flex poison out of veins!",
		VENOMGLAND = "Is weapon of cowards.",
		POISONBALM = "Wolfgang does not fear poison! But will take just in case!",

		SPEAR_POISON = "Is not cowardly if I use it.",
		BLOWDART_POISON = "Is like many tiny punches!",

		SHARX = "I do not trust these sea-doggies.",
		SHARK_FIN = "I lost rest of shark.",
		SHARKFINSOUP = "Is small shark inside?",
		SHARK_TEETHHAT = "Wolfgang's favorite hat!",
		AERODYNAMICHAT = "Keeps mighty legs free to run!",

		IA_MESSAGEBOTTLE = "Wolfgang can't fit meaty hands inside.",
		IA_MESSAGEBOTTLEEMPTY = "Is just a bottle.",
		BURIEDTREASURE = "Is mine!",		

		SAND = "When Wolfgang holds it it slips through his large, strong hands.",
		SANDDUNE = "Tiny, crushable hill.",
		SANDBAGSMALL = "Little bag soaks up waters.",
		SANDBAGSMALL_ITEM = "Is big bag of dirt.",
		SANDCASTLE =
		{
			GENERIC = "Good for stomping!",
			SAND = "Is more sand than castle.",
		},

		SUPERTELESCOPE = "Make eyes strong!",
		TELESCOPE = "I see far!",

		DOYDOY = "Small dumb bird!",
		DOYDOYBABY = "Protect dumb friend, love dumb friend.",
		DOYDOYEGG = "Eat raw, good for mighty muscles!",
		DOYDOYEGG_COOKED = "Stupidly tasty protein.",
		DOYDOYFEATHER = "To remember nice bird by.",
		DOYDOYNEST = "Where dumb friends sleep.",
		TROPICALFAN = "Ha! Is as big as head!",

		PALMTREE =
		{
			BURNING = "I broke it.",
			BURNT = "Is small and broken now.",
			CHOPPED = "Ha! I win!",
			GENERIC = "Such jovial tree.",
		},

		COCONUT = "Wolfgang will crush with bare hands.",
		COCONUT_HALVED = "Wolfgang had no problem smacking hard shell apart.",
		COCONUT_COOKED = "Coconut is cooked.",
		COCONUT_SAPLING = "Is tiny tree! Very small!",
		PALMLEAF = "Big green leaf.",
		PALMLEAF_UMBRELLA = "I do not like tiny umbrella.",
		PALMLEAF_HUT = "The great Wolfgang deserves comfort.",
		LEIF_PALM = "Wood man! Fight me!",

		CRAB =
		{
			GENERIC = "If I punch it, it will be food.",
			HIDDEN = "Dun be shellfish.",
		},

		CRABHOLE = "Crabbit is coward.",

		TRAWLNETDROPPED =
		{
			SOON = "Is really sinking.",
			SOONISH = "Is sinking.",
			GENERIC = "What fishes will I find?",
		},

		TRAWLNET = "Wolfgang will trap you in his net!",
		IA_TRIDENT = "I feel like king of sea.",

		KRAKEN = "Crummy breadeater!",
		KRAKENCHEST = "It dropped something.",
		KRAKEN_TENTACLE = "You have many arms!",
		QUACKENBEAK = "Wolfgang is very proud of bird souvenir!",
		QUACKENDRILL = "Wolfgang will punch bottom of ocean!",
		QUACKERINGRAM = "SMASH!",

		MAGMAROCK = "Is rock left by angry volcano.",
		MAGMAROCK_GOLD = "Is shiny volcano rock.",
		FLAMEGEYSER = "Watch Wolfgang walk over this!",

		TELEPORTATO_SW_RING = "I will carry you.",--unused
		TELEPORTATO_SW_BOX = "What use is tiny box? Wolfgang would like to know.",--unused
		TELEPORTATO_SW_CRANK = "Wolfgang will hold on to this.",--unused
		TELEPORTATO_SW_POTATO = "Hello, spuddy.",--unused
		TELEPORTATO_SW_BASE = "Wolfgang must fix it.",--unused

		PRIMEAPE = "Does not look like even match for me.",
		PRIMEAPEBARREL = "Home of tiny monkey men.",
		MONKEYBALL = "Ha ha! Stupid monkey men.",
		WILBUR_UNLOCK = "Welcome, friend! Ha ha!",--unused
		WILBUR_CROWN = "Crown is falling apart!",--unused

		MERMFISHER = "Let us fight!",
		MERMHOUSE_FISHER = "Ugly house!",
		
		OCTOPUSKING = "Strong creature! Give Wolfgang many good things!",
		OCTOPUSCHEST = "Give Wolfgang your treasures!",

		SWEET_POTATO = "Very sweet!",
		SWEET_POTATO_COOKED = "Is ready to eat!",
		SWEET_POTATO_PLANTED = "Potato is strange color inside.",
		SWEET_POTATO_SEEDS = "Seeds would taste better if they were a potato.",
		SWEET_POTATO_OVERSIZED = "Is mighty and sweet!",
		SWEETPOTATOSOUFFLE = "Wolfgang will eat!",

		BOAT_WOODLEGS = "Is very bad boat.",
		WOODLEGSHAT = "Is head clothes.",
		SAIL_WOODLEGS = "Is scary sail!",

		PEG_LEG = "Is best battle trophy!",
		PIRATEGHOST = "Aaaaaaaaaaahrrrr!!",
		
		WILDBORE = "I will wrestle you.",
		WILDBOREHEAD = "Very gross!",
		WILDBOREHOUSE = "Puny house could not hold me.",

		MANGROVETREE = "Is very thirsty.",
		MANGROVETREE_BURNT = "Could not withstanding even small heat!",

		PORTAL_SHIPWRECKED = "Where do you go?",
		SHIPWRECKED_ENTRANCE = "Is magical portal. Very sandy...",
		SHIPWRECKED_EXIT = "Is magical portal. Very grassy...",

		TIDALPOOL = "Fishies nibble toes when I stick them in.",
		FISHINHOLE = "Is good place to fish for... fish.",
		FISH_TROPICAL = "Is yummy looking.",
		TIDAL_PLANT = "Is plant.",
		MARSH_PLANT_TROPICAL = "Puny shrub.",

		FLUP = "Flup off!",
		BLOWDART_FLUP = "Is workout for my powerful lungs!",

		SEA_LAB = "Wolfgang will learn many splishy splashy things!",
		BUOY = "Ahhh, is marking something.",
		WATERCHEST = "Is for holding seastuff!",
		
		LUGGAGECHEST = "Wolfgang will smash and get treasures!",
		WATERYGRAVE = "I do not like this. Is creepy.",
		SHIPWRECK = "Not mighty enough to float.",
		BARREL_GUNPOWDER = "Do not poke barrel.",
		RAWLING = "Wolfgang plays ball!",
		GRASS_WATER = "Is thirsty grass.",
		KNIGHTBOAT = "Let us fight!",

		DEPLETED_BAMBOOTREE = "Bush is done for.",
		DEPLETED_BUSH_VINE = "Vine bush is look pathetic!",
		DEPLETED_GRASS_WATER = "Maybe grass just need to eat.",
		
		WALLYINTRO_DEBRIS = "It's lost its battle.",
		BOOK_METEOR = "Book make very hot rain.",
		CRATE = "I could smash.",
		SPEAR_LAUNCHER = "Punch from a great distance!",
		MUTATOR_TROPICAL_SPIDER_WARRIOR = "Tiny spider will be crushed in Wolfgang's mighty jaws! Or maybe not.",

		CHESSPIECE_KRAKEN = "Is birdfish Wolfgang punched!",
		--CHESSPIECE_TIGERSHARK = "TEMP, put something here",
		--CHESSPIECE_TWISTER = "TEMP, put something here",
		--CHESSPIECE_SEAL = "TEMP, put something here",

		SLINGSHOTAMMO_LIMESTONE = "Is little bits of junk.",
		SLINGSHOTAMMO_TAR = "Is little bits of junk.",
		SLINGSHOTAMMO_OBSIDIAN = "Is little bits of junk.",

		MS_RECORD_SHIPWRECKED = "Ah yes. Soothing Wolfgang soul.",
		MS_RECORD_SHIPWRECKED_OLD = "Watch Wolfgang dance!",
		MS_RECORD_SHIPWRECKED_BEACHED = "Ah yes. Soothing Wolfgang soul.",

		--SWC
		BOAT_SURFBOARD = "Wolfgang is too top heavy.",
		SURFBOARD_ITEM = "Wolfgang is too top heavy.",

		WALANI = {
            GENERIC = "Is tiny chill lady, %s! Hello!",
            ATTACKER = "Does tiny chill lady want to fight?",
            MURDERER = "%s is no match for Wolfgang!",
            REVIVER = "%s is very nice, tells Wolfgang sea stories!",
            GHOST = "Wolfgang will get heart! Keep taking nap!",
			FIRESTARTER = "Surf lady trying to surf on fire now?",
		},

		WILBUR = {
            GENERIC = "Is tiny monkey man, %s! Hello!",
            ATTACKER = "Ah! Monkey man is try to fight me!",
            MURDERER = "Monkey man %s is killer! Wolfgang is run!",
            REVIVER = "%s is nice monkey man.",
            GHOST = "Wolfgang will get raw monkey heart for you!",
			FIRESTARTER = "Please no. Please no burnings monkey man!",
		},

		WOODLEGS = {
            GENERIC = "Is curvy beard! Hello!",
            ATTACKER = "Does curvy beardman %s want to fight?",
            MURDERER = "Drop sword and fight curvy beardman!",
            REVIVER = "%s is nice man with magnificent beard.",
            GHOST = "Wolfgang will get heart for you, curvy beard!",
			FIRESTARTER = "Be careful! Do not burn, curvy beard!",
		},
	},
}
