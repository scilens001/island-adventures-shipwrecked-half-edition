local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local NO_TAGS = {"ia_boat"}
IAENV.AddSimPostInit(function()
    if TheWorld.ismastersim and Prefabs.storage_robot then
        local CONTAINER_CANT_TAGS = UpvalueHacker.GetUpvalue(Prefabs.storage_robot.fn, "FindContainerWithItem", "CONTAINER_CANT_TAGS")
        for i, tag in ipairs(NO_TAGS) do
            table.insert(CONTAINER_CANT_TAGS, tag)
        end
    end
end)