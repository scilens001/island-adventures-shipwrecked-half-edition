local IAENV = env
GLOBAL.setfenv(1, GLOBAL)

local _AuraTest = nil -- For mod compat
local function AuraTest(inst, target, ...)
    if _AuraTest ~= nil and not _AuraTest(inst, target, ...) then return false end
    return target.components.inventory == nil or not target.components.inventory:HasPoisonGasBlockerEquip()
end

IAENV.AddPrefabPostInit("sporecloud", function(inst)
    if TheWorld.ismastersim then
        if not _AuraTest then
            _AuraTest = inst.components.aura.auratestfn
        end
        inst.components.aura.auratestfn = AuraTest
    end
end)